from django import forms

class MessageForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Name',
        'type' : 'text',
    }))
    message = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'message',
        'type' : 'text',
    }))

