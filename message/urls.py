from django.contrib import admin
from django.urls import path, include
from .views import Index, Conf, delete

app_name = 'message'

urlpatterns = [
    path('', Index, name= 'Index'),
    path('<int:pk>/', delete, name="delete"),
    path('confirmation/', Conf, name= 'confirmation'),
]