from django.shortcuts import render, redirect
from .models import Message
from .forms import MessageForm

def Index(request):
    if request.method=="POST":
        frm = MessageForm(request.POST)
        if frm.is_valid():
            frm2 = Message()
            frm2.name = frm.cleaned_data["name"]
            frm2.message= frm.cleaned_data["message"]
            frm2.save()
        return redirect("/confirmation")
    frm = MessageForm()
    frm2 = Message.objects.all()
    frm_dictio = {
        'frm':frm, #form
        'frm2':frm2 #model/isi formnya
    }
    return render(request,'story7.html',frm_dictio)

def Conf(request):
    msg_input = Message.objects.last()
    conf_dictio = {
        'msg_input': msg_input}
    return render(request, 'conf.html',conf_dictio)

def delete(request, pk):
    if request.method == "POST":
        frm = MessageForm(request.POST)
        if frm.is_valid():
            frm2 = Message()
            frm2.name = frm.cleaned_data["name"]
            frm2.message = frm.cleaned_data["message"]
            frm2.save()
        return redirect("/confirmation")
    else:
        Message.objects.filter(pk = pk).delete()
        frm = MessageForm()
        frm2 = Message.objects.all()
        msg_dictio ={
            'frm' : frm,
            'frm2': frm2
        }
        return render(request, 'story7.html', msg_dictio)

